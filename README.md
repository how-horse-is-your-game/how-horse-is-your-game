## Terraform:
1. (Only once) Run `terraform init` inside the ./terraform directory
2. Run `terraform plan` to see changes
3. Run `terraform apply` to commit changes (make a deployment)

***
## API Docs

### GET /health
- Returns `"Healthy as a 🦄 : )"`

### GET /scores
Returns array of top scores of shape:
```
{
  score: int
  name: string
  id: string
}
```

### POST /scores
Requires body containing
```
{
  name: string
  score: int
}
```

Returns message `"New score by <<name>>: <<score>>"`