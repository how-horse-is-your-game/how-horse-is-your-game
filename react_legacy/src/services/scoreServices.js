export const fetchTopTenScores = () => {
  return fetch('TBD')
    .then(res => ([res.ok, res.json()]))
    .then(([ok, json]) => {
      if(!ok) throw 'Unable to fetch';
      return json;
    });
};

export const createNewScore = (score) => {
  return fetch('TBD' {
    method: 'POST',
    body: JSON.stringify(score), // data can be `string` or {object}!
    headers:{
      'Content-Type': 'application/json'
    } })
    .then(res => ([res.ok, res.json()]))
    .then(([ok, json]) => {
      if(!ok) throw 'Unable to fetch';
      return json;
    });
};

