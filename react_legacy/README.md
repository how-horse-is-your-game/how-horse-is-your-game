## Prereqs:
- node
- webpack

***

## Building for deployment:
Prep to deploy code
1. Run `npm run build`
   - this will bundle files into the `dist` folder
2. Update s3 files
   - navigate to aws s3, `horse-game` bucket
   - replace existing files: `index.html` and both `bundle.xxxxxx` files with what was built in dist. may have to delete current files, and upload new ones