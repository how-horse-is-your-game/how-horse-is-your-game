import Head from 'next/head'
import Layout, { siteTitle } from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import { GetStaticProps } from 'next';

export default function Home({ butts }) {
  console.log("butts", butts, "this is hear because of the getStaticProps function.");
  
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>Horse game is a game n stuff. Have fun... or not : )</p>
      </section>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Game Section</h2>
      </section>
    </Layout>
  )
}

export const getStaticProps: GetStaticProps = async (context) => {
  return {
    props: {
      butts: "( | ), ( | ), ( | )",
      params: context.params || null,
      date: "November 21 1989"
    }
  }
}
