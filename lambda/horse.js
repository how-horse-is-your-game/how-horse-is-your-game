const AWS = require("aws-sdk");

const dynamo = new AWS.DynamoDB.DocumentClient();

const horseScoresTable = "HorseyScores";
const horseTopScoresTable = "HorseyTopScores";
const maxTopScores = 10;

const getTopScores = async () => {
  try {
    const { Items: scores } = await dynamo
      .scan({ TableName: horseTopScoresTable })
      .promise();

    const orderedScores = scores.sort((a, b) => {
      if (b.score > a.score) return 1
      else return -1
    })

    const lowestScore = scores[scores.length - 1]

    return { orderedScores, lowestScore }
  } catch (e) {
    throw new Error(`Error getting top score. Error: ${e.message}`)
  }
}

const putNewScore = async ({ tableName, score, name }) => {
  try {
    const id = `${Date.now()}-${score}-${name}`

    await dynamo
      .put({
        TableName: tableName,
        Item: {
          id,
          score,
          name
        }
      })
      .promise();
  } catch (e) {
    throw new Error(`Error adding score: ${id}. Error: ${e.message}`)
  }
}

const deleteTopScore = async ({ id, score, topScoreCount }) => {
  try {
    if (topScoreCount <= maxTopScores)
      await dynamo
        .delete({
          TableName: horseTopScoresTable,
          Key: { id, score }
        })
        .promise();
  } catch (e) {
    throw new Error(`Error deleting score: ${id}. Error: ${e.message}`)
  }
}

exports.handler = async (event) => {
  let body;
  let statusCode = 200;
  const headers = {
    "Content-Type": "application/json"
  };

  const { orderedScores, lowestScore } = await getTopScores();

  try {
    switch (event.requestContext.resourceId) {
      case "GET /health":
        body = "Healthy as a 🦄 : )";
        break;
      case "GET /scores":
        body = orderedScores
        break;
      case "POST /scores":
        const requestJSON = JSON.parse(event.body);
        const { score, name } = requestJSON;

        const topScoreCount = orderedScores.length

        if (topScoreCount < maxTopScores) {
          await putNewScore({ tableName: horseTopScoresTable, score, name })
        }
        else if (score >= lowestScore.score) {
          await deleteTopScore({ id: lowestScore.id, score: lowestScore.score, topScoreCount })
          await putNewScore({ tableName: horseTopScoresTable, score, name })
        }

        await putNewScore({ tableName: horseScoresTable, score, name })

        body = `New score by ${name}: ${score}`;
        break;
      default:
        throw new Error(`Unsupported route: "${event.routeKey}". event: ${JSON.stringify(event.requestContext.resourceId)}`);
    }
  } catch (err) {
    statusCode = 400;
    body = err.message;
  } finally {
    body = JSON.stringify(body);
  }

  return {
    statusCode,
    body,
    headers,
  };
};
