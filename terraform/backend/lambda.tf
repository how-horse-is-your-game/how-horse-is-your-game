# ************************************************
# *************** S3 lambda Setup ****************
# ************************************************
resource "random_pet" "bucket_name" {
  prefix = "learn-terraform-functions"
  length = 4
}

resource "aws_s3_bucket" "code_bucket" {
  bucket = random_pet.bucket_name.id

  force_destroy = true
}

data "archive_file" "zipped_file" {
  type = "zip"

  source_dir  = "../lambda"
  output_path = "../lambda/horse.zip"
}

resource "aws_s3_object" "horse_object" {
  bucket = aws_s3_bucket.code_bucket.id

  key    = "horse.zip"
  source = data.archive_file.zipped_file.output_path

  etag = filemd5(data.archive_file.zipped_file.output_path)
}

# ************************************************
# ******************* Lambda *********************
# ************************************************

resource "aws_lambda_function" "horse_lambda" {
  function_name = "HorseLambda"

  s3_bucket = aws_s3_bucket.code_bucket.id
  s3_key    = aws_s3_object.horse_object.key

  runtime = "nodejs12.x"
  handler = "horse.handler"

  source_code_hash = data.archive_file.zipped_file.output_base64sha256

  role = aws_iam_role.lambda_policy.arn
}

# ************************************************
# ************* Cloudwatch and iam ***************
# ************************************************

resource "aws_cloudwatch_log_group" "horses_mouth" {
  name = "/aws/lambda/${aws_lambda_function.horse_lambda.function_name}"

  retention_in_days = 30
}

resource "aws_iam_role" "lambda_policy" {
  name = "serverless_lambda"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy" {
  role       = aws_iam_role.lambda_policy.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
