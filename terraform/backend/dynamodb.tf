# ************************************************
# ******************** Tables ********************
# ************************************************

# Table for all scores
resource "aws_dynamodb_table" "horse_score_table" {
  name           = "HorseyScores"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "id"
  range_key      = "score"

  attribute {
    name = "id"
    type = "S"
  }
  
  attribute {
    name = "score"
    type = "N"
  }
}

# Tables for only top scores
resource "aws_dynamodb_table" "horse_top_score_table" {
  name           = "HorseyTopScores"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "id"
  range_key      = "score"

  attribute {
    name = "id"
    type = "S"
  }
  
  attribute {
    name = "score"
    type = "N"
  }
}

# ************************************************
# ******************* Policies *******************
# ************************************************

resource "aws_iam_policy" "lambda_db_policy" {
  name = "lambda_db_policy"

  policy = jsonencode({  
    Version: "2012-10-17",
    Statement:[{
      Effect: "Allow",
      Action: [
      "dynamodb:Scan",
      "dynamodb:PutItem",
      ],
      Resource: aws_dynamodb_table.horse_score_table.arn
    },
    {
      Effect: "Allow",
      Action: [
      "dynamodb:Scan",
      "dynamodb:PutItem",
      "dynamodb:DeleteItem"
      ],
      Resource: aws_dynamodb_table.horse_top_score_table.arn
    }]
  })
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.lambda_policy.name
  policy_arn = aws_iam_policy.lambda_db_policy.arn
}