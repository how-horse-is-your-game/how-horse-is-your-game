# ************************************************
# ******* API, Stage and Integration *************
# ************************************************
resource "aws_apigatewayv2_api" "horse_api" {
  name          = "serverless_lambda_gw"
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_stage" "api_stage" {
  api_id = aws_apigatewayv2_api.horse_api.id

  name        = "serverless_lambda_stage"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_gw.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
}

resource "aws_apigatewayv2_integration" "horse_integration" {
  api_id = aws_apigatewayv2_api.horse_api.id

  integration_uri    = aws_lambda_function.horse_lambda.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

# ************************************************
# ******************* Routes *********************
# ************************************************

resource "aws_apigatewayv2_route" "health_route" {
  api_id = aws_apigatewayv2_api.horse_api.id

  route_key = "GET /health"
  target    = "integrations/${aws_apigatewayv2_integration.horse_integration.id}"
}

resource "aws_apigatewayv2_route" "get_scores" {
  api_id = aws_apigatewayv2_api.horse_api.id

  route_key = "GET /scores"
  target    = "integrations/${aws_apigatewayv2_integration.horse_integration.id}"
}

resource "aws_apigatewayv2_route" "post_score" {
  api_id = aws_apigatewayv2_api.horse_api.id

  route_key = "POST /scores"
  target    = "integrations/${aws_apigatewayv2_integration.horse_integration.id}"
}

# ************************************************
# ********** Cloudwath and Permissions ***********
# ************************************************

resource "aws_cloudwatch_log_group" "api_gw" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.horse_api.name}"

  retention_in_days = 30
}

resource "aws_lambda_permission" "api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.horse_lambda.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.horse_api.execution_arn}/*/*"
}
